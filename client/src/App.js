import React, { useState, useEffect } from "react";
import { Button } from "antd";
import "./App.css";
import { Input, Row, Col, Icon } from "antd";
import axios from "axios";
import ReactAudioPlayer from "react-audio-player";
import { List, Avatar } from 'antd';

const App = () => {
  const { TextArea } = Input;
  const [inputText, setInputText] = useState("");
  const [currentAudio, setCurrentAudio] = useState();
  const [isSending, setIsSending] = useState(false);
  const [audios, setAudios] = useState();

  useEffect(() => {
   
    if (localStorage.length > 0 ) {
      setAudios({...localStorage});
    }
  }, []);
  console.log('these are the audio',audios)
  return (
    <div className="container">
      <h1 className="title">Text to speech using IBM</h1>
      <TextArea
        placeholder="Type some text to convert to speech"
        rows={5}
        cols={8}
        maxLength={50}
        className="text-area"
        onChange={(event) => {
          event.preventDefault();
          if (event.target.value === "") {
            setIsSending(false);
          } else {
            setIsSending(true);
            setInputText(event.target.value);
          }
        }}
      />
      <Row gutter={[32, 32]} align="middle" className="player">
        <Col span={4}>
          <Button
            className="button"
            type="primary"
            disabled={!isSending}
            size="large"
            onClick={(event) => {
              event.preventDefault();
              const localStorageItem = localStorage.getItem(inputText);

              if (localStorageItem === null && localStorageItem != inputText) {
                axios
                  .post("http://localhost:9000/text-to-speech/", inputText, {
                    responseType: "arraybuffer",
                  })
                  .then((response) => {
                    const blob = new Blob([response.data], {
                      type: "audio/wav",
                    });
                    console.log('thisis the blob',blob);
                    localStorage.setItem(inputText, JSON.stringify(blob));
                    const url = window.URL.createObjectURL(blob);
                    window.audio = new Audio();
                    window.audio.src = url;
                    
                    setCurrentAudio(url);
                  })
                  .catch((error) => {
                    console.error("error", error);
                  });
              } else {
                Object.keys(localStorage).forEach((key) => {
                  if (key === inputText) {
                    const url = localStorage.getItem(key);
                    window.audio = new Audio();
                    window.audio.src = url;
                    setCurrentAudio(url);
                  }
                });
              }
            }}
          >
            Synthesize
          </Button>
        </Col>

        <Col span={4}>
              
        </Col>
        <Col span={2}>
          <ReactAudioPlayer src={currentAudio} controls autoPlay />
        </Col>
      </Row>
    </div>
  );
};

export default App;
