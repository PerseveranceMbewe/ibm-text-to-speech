var TextToSpeechV1 = require("ibm-watson/text-to-speech/v1");
var express = require("express");
var router = express.Router();
const { IamAuthenticator } = require("ibm-watson/auth");
const fs = require("fs");
const { response } = require("express");
const path = require('path');

const api_audio_get = async (req, res) => {
  const inputText = JSON.stringify(req.body);
  const textToSpeech = new TextToSpeechV1({
    authenticator: new IamAuthenticator({
      apikey: "MRJOLw4G9GvJorC3PABXZI3TGUW9r19zKqF-C7Ej0MST",
    }),
    serviceUrl:
      "https://api.eu-gb.text-to-speech.watson.cloud.ibm.com/instances/218158b8-959a-4697-82b2-18b73e03224f",
  });

  const synthesizeParams = {
    text: inputText,
    accept: "audio/wav",
    voice: "en-US_AllisonV3Voice",
  };
  textToSpeech
    .synthesize(synthesizeParams)
    .then((response) => {
      return textToSpeech.repairWavHeaderStream(response.result);
    })
    .then((buffer) => {
      fs.writeFileSync("hello_world.wav", buffer);
      var filePath = path.join(__dirname, "../hello_world.wav");
      var stat = fs.statSync(filePath);

      res.writeHead(200, {
        "Content-Type": "audio/wav",
        "Content-Length": stat.size,
      });

      var readStream = fs.createReadStream(filePath);
      readStream.pipe(res);
    })
    .catch((err) => {
      console.log("error:", err);
    });
};

router.post("/", api_audio_get);

module.exports = router;
